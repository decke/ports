--- src/disk.c.orig	2024-01-31 20:08:46 UTC
+++ src/disk.c
@@ -158,7 +158,7 @@ msdos_disk_get_partition_info (int fd, int write_signa
 	} else if (num == 0) {
 		/* Whole disk */
 		*start = 0;
-		ioctl(fd, BLKGETSIZE, &disk_size);
+		//ioctl(fd, BLKGETSIZE, &disk_size); XXX
 		*size = disk_size;
 	} else if (num >= 1 && num <= 4) {
 		/* Primary partition */
