--- src/efivarfs.c.orig	2024-01-31 20:08:46 UTC
+++ src/efivarfs.c
@@ -9,21 +9,19 @@
 #include <err.h>
 #include <errno.h>
 #include <fcntl.h>
-#include <linux/magic.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <sys/mman.h>
 #include <sys/types.h>
+#include <sys/param.h>
+#include <sys/mount.h>
 #include <sys/stat.h>
 #include <sys/uio.h>
-#include <sys/vfs.h>
 #include <unistd.h>
 
 #include "efivar.h"
 
-#include <linux/fs.h>
-
 #ifndef EFIVARFS_MAGIC
 #  define EFIVARFS_MAGIC 0xde5e81e4
 #endif
@@ -102,6 +100,7 @@ efivarfs_set_fd_immutable(int fd, int immutable)
 static int
 efivarfs_set_fd_immutable(int fd, int immutable)
 {
+#if 0
 	unsigned int flags;
 	int rc = 0;
 
@@ -124,8 +123,14 @@ efivarfs_set_fd_immutable(int fd, int immutable)
 	}
 
 	return rc;
+#else
+	(void)fd;
+	(void)immutable;
+	return -1;
+#endif
 }
 
+#if 0
 static int
 efivarfs_make_fd_mutable(int fd, unsigned long *orig_attrs)
 {
@@ -146,6 +151,7 @@ efivarfs_make_fd_mutable(int fd, unsigned long *orig_a
 
 	return 0;
 }
+#endif
 
 static int
 efivarfs_set_immutable(char *path, int immutable)
@@ -328,8 +334,8 @@ efivarfs_set_variable(efi_guid_t guid, const char *nam
 	uint8_t *buf;
 	int rfd = -1;
 	struct stat rfd_stat;
-	unsigned long orig_attrs = 0;
-	int restore_immutable_fd = -1;
+//	unsigned long orig_attrs = 0;
+//	int restore_immutable_fd = -1;
 	int wfd = -1;
 	int open_wflags;
 	int ret = -1;
@@ -375,10 +381,12 @@ efivarfs_set_variable(efi_guid_t guid, const char *nam
 			goto err;
 		}
 
+#if 0
 		/* if the file is indeed immutable, clear and remember it */
 		if (efivarfs_make_fd_mutable(rfd, &orig_attrs) == 0 &&
 		    (orig_attrs & FS_IMMUTABLE_FL))
 			restore_immutable_fd = rfd;
+#endif
 	}
 
 	/*
@@ -414,9 +422,11 @@ efivarfs_set_variable(efi_guid_t guid, const char *nam
 	 * immediately, and the write() below would fail otherwise.
 	 */
 	if (rfd == -1) {
+#if 0
 		if (efivarfs_make_fd_mutable(wfd, &orig_attrs) == 0 &&
 		    (orig_attrs & FS_IMMUTABLE_FL))
 			restore_immutable_fd = wfd;
+#endif
 	} else {
 		/* make sure rfd and wfd refer to the same file */
 		struct stat wfd_stat;
@@ -452,7 +462,9 @@ err:
 	if (ret == -1 && rfd == -1 && wfd != -1 && unlink(path) == -1)
 		efi_error("failed to unlink %s", path);
 
+#if 0
 	ioctl(restore_immutable_fd, FS_IOC_SETFLAGS, &orig_attrs);
+#endif
 
 	if (wfd >= 0)
 		close(wfd);
