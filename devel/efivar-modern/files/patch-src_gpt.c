--- src/gpt.c.orig	2024-01-31 20:08:46 UTC
+++ src/gpt.c
@@ -115,12 +115,12 @@ _get_num_sectors(int filedes)
 	uint64_t bytes=0;
 	int rc;
 	if (kernel_has_blkgetsize64()) {
-		rc = ioctl(filedes, BLKGETSIZE64, &bytes);
+		rc = 1;//ioctl(filedes, BLKGETSIZE64, &bytes); XXX
 		if (!rc)
 			return bytes / get_sector_size(filedes);
 	}
 
-	rc = ioctl(filedes, BLKGETSIZE, &sectors);
+	rc = 1;//ioctl(filedes, BLKGETSIZE, &sectors); XXX
 	if (rc)
 		return 0;
 
