--- src/linux.c.orig	2024-01-31 20:08:46 UTC
+++ src/linux.c
@@ -12,17 +12,12 @@
 #include <fcntl.h>
 #include <inttypes.h>
 #include <limits.h>
-#include <linux/ethtool.h>
-#include <linux/version.h>
-#include <linux/sockios.h>
 #include <net/if.h>
-#include <scsi/scsi.h>
 #include <stdbool.h>
 #include <stdio.h>
 #include <sys/ioctl.h>
 #include <sys/mount.h>
 #include <sys/socket.h>
-#include <sys/sysmacros.h>
 #include <sys/types.h>
 #include <sys/param.h>
 #include <sys/stat.h>
@@ -633,6 +628,12 @@ make_mac_path(uint8_t *buf, ssize_t size, const char *
 ssize_t HIDDEN
 make_mac_path(uint8_t *buf, ssize_t size, const char * const ifname)
 {
+	(void)buf;
+	(void)size;
+	(void)ifname;
+	return -1;
+	// XXX
+#if 0
 	struct ifreq ifr;
 	struct ethtool_drvinfo drvinfo = { 0, };
 	int fd = -1, rc;
@@ -692,6 +693,7 @@ err:
 	if (fd >= 0)
 	        close(fd);
 	return ret;
+#endif
 }
 
 /************************************************************
@@ -707,7 +709,8 @@ get_sector_size(int filedes)
 {
 	int rc, sector_size = 512;
 
-	rc = ioctl(filedes, BLKSSZGET, &sector_size);
+	(void)filedes;
+	//rc = ioctl(filedes, BLKSSZGET, &sector_size); XXX
 	if (rc)
 	        sector_size = 512;
 	return sector_size;
