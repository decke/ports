--- src/thread-test.c.orig	2024-01-31 20:08:46 UTC
+++ src/thread-test.c
@@ -6,7 +6,6 @@
 
 #include "fix_coverity.h"
 
-#include <alloca.h>
 #include <efivar.h>
 #include <err.h>
 #include <errno.h>
@@ -125,7 +124,7 @@ usage(int ret)
 		"Help options:\n"
 		"  -?, --help                        Show this help message\n"
 		"      --usage                       Display brief usage message\n",
-		program_invocation_short_name);
+		getprogname());
 	exit(ret);
 }
 
