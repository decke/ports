--- src/CMakeLists.txt.orig	2024-11-01 17:35:57 UTC
+++ src/CMakeLists.txt
@@ -194,7 +194,7 @@ if (WITH_WEBSOCKETS)
 
 if (WITH_WEBSOCKETS)
 	if (STATIC_WEBSOCKETS)
-		set (MOSQ_LIBS ${MOSQ_LIBS} websockets_static)
+		set (MOSQ_LIBS ${MOSQ_LIBS} websockets)
 		if (WIN32)
 			set (MOSQ_LIBS ${MOSQ_LIBS} iphlpapi)
 			link_directories(${mosquitto_SOURCE_DIR})
